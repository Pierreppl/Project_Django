var gulp = require('gulp');
var less = require('gulp-less');
var minify = require('gulp-clean-css');
var rename = require('gulp-rename');

gulp.task('dev', function(){
    return gulp.src('assets/less/main.less')
      .pipe(less())
      .pipe(gulp.dest('djangoCompany/lesCompagnies/static/css'))
});

gulp.task('dist', function(){
    return gulp.src('assets/less/main.less')
    	.pipe(less())
      .pipe(minify())
      .pipe(rename('main.min.css'))
    	.pipe(gulp.dest("djangoCompany/lesCompagnies/static/css"));
});

gulp.task('fonts', function(){
  return gulp.src('bower_components/font-awesome/fonts/*')
    .pipe(gulp.dest('djangoCompany/lesCompagnies/static/fonts'))
});

gulp.task('watch', function(){
  return gulp.watch('assets/less/*.less', ['dev']);
});
