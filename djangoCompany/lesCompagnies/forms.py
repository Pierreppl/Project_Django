from django.forms import ModelForm, TextInput, NumberInput, HiddenInput,ModelChoiceField, Select, DateInput,ChoiceField,CharField
from django import forms
from lesCompagnies.models import Entreprise, Contact, Taxe, Contacter
from djangoCompany import settings
from datetime import date

# Formulaire pour création et modification d'une entreprise
class FormulaireEntreprise(ModelForm):
    def __init__(self,*args,**kwargs):
        super(FormulaireEntreprise,self).__init__(*args,**kwargs)
        self.fields['nom'].label = "Nom "
        self.fields['adresse'].label = "Adresse "
        self.fields['codePostal'].label = "Code postal "
        self.fields['ville'].label = "Ville "
        self.fields['pays'].label = "Pays "

    class Meta:
        model = Entreprise
        fields = ('nom','adresse','codePostal', 'ville', 'pays','telephone','latitude','longitude')
        widgets = {
            'nom':TextInput(attrs={'class':'form-control','required':'true'}),
            'adresse':TextInput(attrs={'class':'form-control','required':'true'}),
            'codePostal':TextInput(attrs={'class':'form-control','required':'true'}),
            'ville':TextInput(attrs={'class':'form-control','required':'true'}),
            'pays':TextInput(attrs={'class':'form-control','required':'true'}),
            'telephone':TextInput(attrs={'class':'form-control','required':'true'}),
            'pays':TextInput(attrs={'class':'form-control','required':'true'}),
            'latitude':HiddenInput(attrs={'class':'form-control','value':'0'}),
            'longitude':HiddenInput(attrs={'class':'form-control','value':'0'}),
        }

choixRechercheEntreprise = (
                    ('nomEntreprise', 'Nom de l\'entreprise'),
                    ('villeEntreprise', 'Ville de l\'entreprise'),
                    ('adresseEntreprise', 'Adresse de l\'entreprise'),
                    ('codePostalEntreprise', 'Code Postal de l\'entreprise'),
                    ('paysEntreprise', 'Pays de l\'entreprise'),
                    ('numeroTelephoneEntreprise', 'Numéro de téléphone de l\'entreprise'),
                )

# Formulaire pour la recherche d'entreprise
class FormulaireRechercheEntreprise(forms.Form):
    choixRecherche = ChoiceField(label='Recherche', widget=Select(attrs={'class':'form-control','required':'true'}), choices=choixRechercheEntreprise)
    champRecherche = CharField(label='',widget=TextInput(attrs={'class':'form-control','required':'true','placeholder':'Votre recherche'}))

choixRechercheContact = (
                    ('entrepriseContact', 'Nom de l\'entreprise'),
                    ('nomContact', 'Nom du contact'),
                    ('libelleContact', 'Libellé du contact'),
                    ('emailContact', 'Adresse email du contact'),
                )

# Formulaire  pour la recherche d'un contact
class FormulaireRechercheContact(forms.Form):
    choixRecherche = ChoiceField(label='Recherche', widget=Select(attrs={'class':'form-control','required':'true'}), choices=choixRechercheContact)
    champRecherche = CharField(label='',widget=TextInput(attrs={'class':'form-control','required':'true','placeholder':'Votre recherche'}))


# Formulaire pour création et modification d'un contact
class FormulaireContact(ModelForm):
    def __init__(self,*args,**kwargs):
        super(FormulaireContact,self).__init__(*args,**kwargs)
        self.fields['nomContact'].label = "Nom "
        self.fields['libelleContact'].label = "Libellé "
        self.fields['emailContact'].label = "Adresse email "
        self.fields['entrepriseContact']=ModelChoiceField(queryset=Entreprise.objects.all().order_by('nom'),empty_label=" ", widget=Select(attrs={'class':'selectpicker form-control','required':'true','data-live-search':'true'}))
        self.fields['entrepriseContact'].label = " Entreprise associé "

    class Meta:
        model = Contact
        fields = ('nomContact','libelleContact','emailContact', 'entrepriseContact')
        widgets = {
            'nomContact':TextInput(attrs={'class':'form-control','required':'true'}),
            'libelleContact':TextInput(attrs={'class':'form-control','required':'true'}),
            'emailContact':TextInput(attrs={'class':'form-control','required':'true'}),

        }

# Formulaire pour création et modification d'une taxe
class FormulaireTaxe(ModelForm):
    def __init__(self,*args,**kwargs):
        super(FormulaireTaxe,self).__init__(*args,**kwargs)
        self.fields['dateTaxe'].label = "Date"
        self.fields['entrepriseTaxe']=ModelChoiceField(queryset=Entreprise.objects.all().order_by('nom'),empty_label=" ", widget=Select(attrs={'class':'selectpicker form-control','required':'true','data-live-search':'true'}))
        self.fields['entrepriseTaxe'].label = " Entreprise associé "

    class Meta:
        model = Taxe
        fields = ('dateTaxe','entrepriseTaxe')
        widgets = {
            'dateTaxe':DateInput(attrs={'class':'form-control','required':'true'}),

        }
ChoixModalite = (
    ('Email', 'Email'),
    ('Téléphone', 'Téléphone'),
    ('Courriel', 'Courriel'),
)

# Formulaire pour création et modification d'un contacter
class FormulaireContacter(ModelForm):
    def __init__(self,*args,**kwargs):
        leContacter = kwargs.pop('leContacter')
        super(FormulaireContacter,self).__init__(*args,**kwargs)
        self.fields['expediteurContacter'].label = "Expéditeur"
        self.fields['modaliteContacter']= ChoiceField(label='Modalité', choices=ChoixModalite, widget=Select(attrs={'class':'selectpicker form-control','required':'true'}))
        self.fields['entrepriseContacter']=ModelChoiceField(queryset=Entreprise.objects.all().order_by('nom'),empty_label=" ", widget=Select(attrs={'class':'selectpicker form-control','required':'true','data-live-search':'true'}))
        self.fields['entrepriseContacter'].label = "Entreprise contacté"
        self.fields['personneContacter']=ModelChoiceField(queryset=Contact.objects.select_related().filter(entrepriseContact=leContacter), widget=Select(attrs={'class':'selectpicker form-control','required':'true','data-live-search':'true'}))
        self.fields['personneContacter'].label = "Personne contacté"

    class Meta:
        model = Contacter
        fields = ('expediteurContacter','modaliteContacter','entrepriseContacter','personneContacter')
        widgets = {
            'expediteurContacter':TextInput(attrs={'class':'form-control','required':'true'}),

        }
