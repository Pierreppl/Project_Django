from django.contrib import admin
from lesCompagnies.models import Entreprise, Contact, Pays, VillesDeFrance


class EntrepriseAdmin(admin.ModelAdmin):
    list_display=('nom','adresse','codePostal','ville','pays','telephone','latitude','longitude')

class ContactAdmin(admin.ModelAdmin):
    list_display=('nomContact','libelleContact','emailContact','entrepriseContact')

class PaysAdmin(admin.ModelAdmin):
    list_display=('code','alpha2','alpha3','nom_en_gb', 'nom_fr_fr')

class VillesDeFranceAdmin(admin.ModelAdmin):
    list_display=('id','departement','nom', 'nom_reel','code_postal','commune','longitude_deg','latitude_deg','longitude_grd','latitude_grd','longitude_dms','latitude_dms')

admin.site.register(Entreprise,EntrepriseAdmin)
admin.site.register(Contact,ContactAdmin)
admin.site.register(Pays,PaysAdmin)
admin.site.register(VillesDeFrance,VillesDeFranceAdmin)
